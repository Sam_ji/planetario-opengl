/*
 * cara.cpp
 *
 *  Created on: 09/10/2014
 *      Author: salabeta
 */

#include "cara.h"

cara::cara() {
	A = B = C = 0;
}

int cara::getA() const {
	return A;
}

void cara::setA(int a) {
	A = a;
}

int cara::getB() const {
	return B;
}

void cara::setB(int b) {
	B = b;
}

int cara::getC() const {
	return C;
}

cara::cara(const int A, const int B, const int C) {
	setA(A);
	setB(B);
	setC(C);
}

cara::cara(const int a, const int b, const int c, const TNormal norm) {
	A = a;
	B = b;
	C = c;
	normal = norm;
}

const TNormal& cara::getNormal() const {
	return normal;
}

void cara::setNormal(const TNormal& normal) {
	this->normal = normal;
}

void cara::setC(int c) {
	C = c;
}

cara::~cara() {

}

