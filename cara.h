/*
 * cara.h
 *
 *  Created on: 09/10/2014
 *      Author: salabeta
 */

#include "punto3D.h"

#ifndef CARA_H_
#define CARA_H_
typedef struct TNormal{
	float x;
	float y;
	float z;

}TNormal;

class cara {

private:
	int A;
	int B;
	int C;
	TNormal normal;


public:
	cara();
	cara(const int A, const int B, const int C);
	cara(const int a, const int b, const int c, const TNormal norm);
	virtual ~cara();
	int getA() const;
	void setA(int a);
	int getB() const;
	void setB(int b);
	int getC() const;
	void setC(int c);
	const TNormal& getNormal() const;
	void setNormal(const TNormal& normal);
};

#endif /* CARA_H_ */
