/*
 * Modelo3D.cpp
 *
 *  Created on: 09/10/2014
 *      Author: salabeta
 */

#include "Modelo3D.h"

void DrawCircle(float r)
{
	int num_segments = 500;
	glDisable(GL_LIGHTING);
	glBegin(GL_LINE_LOOP);
	for(int ii = 0; ii < num_segments; ii++)
	{
		float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments);//get the current angle

		float x = r * cosf(theta);//calculate the x component
		float y = r * sinf(theta);//calculate the y component

		glVertex2f(x , y );//output vertex

	}
	glEnd();
	glEnable(GL_LIGHTING);
}


Modelo3D::Modelo3D() {
	setNumCaras(0);
	setNumVertices(0);
	alfa = 0.0;
	beta = 0.0;
	for (int i = 0; i < 3; i++) {
		Model_color[i] = 0.0;

	}
	ListaCaras.clear();
	setVector4(material_emission, 1.0, 0.5, 0.0, 1.0);
	setVector4(material_specular, 0.3, 1.0, 0.5, 0.2);
	setPeriodo(0);
	setVelocidad(0);
	Load_Model();
}

int Modelo3D::getNumCaras() const {
	return _NumCaras;
}

void Modelo3D::setNumCaras(int numCaras) {
	_NumCaras = numCaras;
}

int Modelo3D::getNumVertices() const {
	return _NumVertices;
}

Modelo3D::Modelo3D(const int ncaras, const int nvertices) {
	setNumCaras(ncaras);
	setNumVertices(nvertices);
}

void Modelo3D::setNumVertices(int numVertices) {
	_NumVertices = numVertices;
}

Modelo3D::~Modelo3D() {

}

void Modelo3D::Load_Model() {
	char fileName[50] = "dat/Esfera.asc";
	FILE *fich;
	int NVertex, NFaces, VertexNumber, FaceNumber, N, A, B, C;
	float X, Y, Z, ax, ay, az, bx, by, bz, len;
	TNormal Normal;
	char cad1[20], cad2[20], cad3[20], cad4[20];
	char cadena[100]; // Lo suf. larga para leer una línea

	if ((fich = fopen(fileName, "r")) == NULL) // open for reading  // open for reading
	{
		cout << " Error en la apertura. Es posible que el fichero no exista \n "
				<< endl;
		exit(1);
	}
	while (fgets(cadena, 100, fich) != NULL) {
		if (strncmp(cadena, "Named", 5) == 0) // Nvertex and NFaces in file
				{
			fscanf(fich, "%[Tri-mesh A-Za-z:-,: ]%d%[ ]%[Faces]:%d\n", cad1,
					&NVertex, cad2, cad3, &NFaces);
			setNumCaras(NFaces);
			setNumVertices(NVertex);
		}
		ListaCaras.resize(getNumCaras());
		ListaPuntos3D.resize(getNumVertices());
		if (strncmp(cadena, "Vertex list:", 12) == 0) // Vertex List in file
			for (N = 1; N <= NVertex; N++) {
				fscanf(fich, "%[A-Za-z ]%d: %[X:] %f %[Y:] %f %[Z:] %f    \n",
						cad1, &VertexNumber, cad2, &X, cad3, &Y, cad4, &Z);
				ListaPuntos3D[VertexNumber] = punto3D(X, Y, Z);

			}
		if (strncmp(cadena, "Face list:", 10) == 0) // Face List in model file
			for (N = 0; N < NFaces; N++) {
				fscanf(fich, "%[Face]%d: %[A:]%d %[B:]%d %[C:]%d\n", cad1,
						&FaceNumber, cad2, &A, cad3, &B, cad4, &C);
				fgets(cadena, 100, fich);
				// Cálculo del vector normal a cada cara (Nx,Ny,Nz)........NEW¡¡¡¡
				ListaCaras[FaceNumber] = cara(A, B, C, Normal);
				ax = ListaPuntos3D[ListaCaras[FaceNumber].getA()].getX()
						- ListaPuntos3D[ListaCaras[FaceNumber].getB()].getX(); //  X[A] - X[B];
				ay = ListaPuntos3D[ListaCaras[FaceNumber].getA()].getY()
						- ListaPuntos3D[ListaCaras[FaceNumber].getB()].getY(); //  Y[A] - Y[B];
				az = ListaPuntos3D[ListaCaras[FaceNumber].getA()].getZ()
						- ListaPuntos3D[ListaCaras[FaceNumber].getB()].getZ(); //  Z[A] - Z[B];
				bx = ListaPuntos3D[ListaCaras[FaceNumber].getB()].getX()
						- ListaPuntos3D[ListaCaras[FaceNumber].getC()].getX(); //  X[B] - X[C];
				by = ListaPuntos3D[ListaCaras[FaceNumber].getB()].getY()
						- ListaPuntos3D[ListaCaras[FaceNumber].getC()].getY(); //  Y[B] - Y[C];
				bz = ListaPuntos3D[ListaCaras[FaceNumber].getB()].getZ()
						- ListaPuntos3D[ListaCaras[FaceNumber].getC()].getZ(); //  Z[B] - Z[C];
				Normal.x = (ay * bz) - (az * by);
				Normal.y = (az * bx) - (ax * bz);
				Normal.z = (ax * by) - (ay * bx);
				len = sqrt(
						(Normal.x * Normal.x) + (Normal.y * Normal.y)
								+ (Normal.z * Normal.z));
				Normal.x = Normal.x / len;
				Normal.y = Normal.y / len;
				Normal.z = Normal.z / len;
				ListaCaras[FaceNumber] = cara(A, B, C, Normal);
			}
	}
	fclose(fich);

}

void Modelo3D::setMaterialEmision(GLfloat a, GLfloat b, GLfloat c, GLfloat d) {
	material_emission[0] = a;
	material_emission[1] = b;
	material_emission[2] = c;
	material_emission[3] = d;
}

void Modelo3D::setMaterialSpec(GLfloat a, GLfloat b, GLfloat c, GLfloat d) {
	material_specular[0] = a;
	material_specular[1] = b;
	material_specular[2] = c;
	material_specular[3] = d;
}

float Modelo3D::getEscala() const {
	return escala;
}

void Modelo3D::setEscala(float escala) {
	this->escala = escala;
}

void Modelo3D::setPosicion(float a, float b, float c) {
	posicion[0] = a;
	posicion[1] = b;
	posicion[2] = c;
}

const float* Modelo3D::getPosicion() const {
	return posicion;
}

void Modelo3D::preparaMaterial() {
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_EMISSION, material_emission);
	glMaterialf(GL_FRONT, GL_SHININESS, 500);
}

void Modelo3D::Draw_Model(float scale, long tiempo) {
	int FaceNumber, NFaces;
	int A, B, C;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glScalef(scale, scale, scale);
	DrawCircle(posicion[0]);

	glRotatef(tiempo*velocidad,0.0f,0.0f,1.0f);
	glTranslatef(posicion[0], posicion[1], posicion[2]);
	//glRotatef(tiempo*self_rot_speed,0.0f,1.0f,0.0f);

	preparaMaterial();
	glShadeModel(GL_SMOOTH);


	glBegin(GL_TRIANGLES);
	NFaces = getNumCaras();
	for (FaceNumber = 0; FaceNumber < NFaces; FaceNumber++) {
		glNormal3f(ListaPuntos3D[ListaCaras[FaceNumber].getA()].getX(),
				ListaPuntos3D[ListaCaras[FaceNumber].getA()].getY(),
				ListaPuntos3D[ListaCaras[FaceNumber].getA()].getZ());

		glVertex3f(ListaPuntos3D[ListaCaras[FaceNumber].getA()].getX(),
				ListaPuntos3D[ListaCaras[FaceNumber].getA()].getY(),
				ListaPuntos3D[ListaCaras[FaceNumber].getA()].getZ());

		glNormal3f(ListaPuntos3D[ListaCaras[FaceNumber].getB()].getX(),
				ListaPuntos3D[ListaCaras[FaceNumber].getB()].getY(),
				ListaPuntos3D[ListaCaras[FaceNumber].getB()].getZ());

		glVertex3f(ListaPuntos3D[ListaCaras[FaceNumber].getB()].getX(),
				ListaPuntos3D[ListaCaras[FaceNumber].getB()].getY(),
				ListaPuntos3D[ListaCaras[FaceNumber].getB()].getZ());

		glNormal3f(ListaPuntos3D[ListaCaras[FaceNumber].getC()].getX(),
				ListaPuntos3D[ListaCaras[FaceNumber].getC()].getY(),
				ListaPuntos3D[ListaCaras[FaceNumber].getC()].getZ());

		glVertex3f(ListaPuntos3D[ListaCaras[FaceNumber].getC()].getX(),
				ListaPuntos3D[ListaCaras[FaceNumber].getC()].getY(),
				ListaPuntos3D[ListaCaras[FaceNumber].getC()].getZ());
	}
	glEnd();


	//Estos son los ejes pintados

}

void Modelo3D::setModelColor(float c1, float c2, float c3) {
	Model_color[0] = c1;
	Model_color[1] = c2;
	Model_color[2] = c3;
}
//ÑAPA
void Modelo3D::setVector4(GLfloat *v, GLfloat v0, GLfloat v1, GLfloat v2,
		GLfloat v3) {
	v[0] = v0;
	v[1] = v1;
	v[2] = v2;
	v[3] = v3;
}

float Modelo3D::getVelocidad() const {
	return velocidad;
}

float Modelo3D::getPeriodo() const {
	return periodo;
}

void Modelo3D::setPeriodo(float periodo) {
	this->periodo = periodo;
	if(periodo != 0)
		velocidad = 2*3.141592/periodo;
}

void Modelo3D::setVelocidad(float velocidad) {
	this->velocidad = velocidad;
}
