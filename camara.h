#include </usr/include/GL/glut.h>
#include </usr/include/GL/gl.h>
#include </usr/include/GL/glu.h>


class camara{
  private: 
    float coordenadas[9];
    
public:
    camara();
    void iniciaCamara();
    void setCoordenada(const int index, const float value);
    void setCoordenadas(const float a, const float b, const float c, const float d, const float e, const float f, const float g, const float h, const float i);
  
};