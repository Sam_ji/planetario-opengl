#include "camara.h"



camara::camara()
{
  // Inicializamos todas las cámaras a cero.
  for (int i = 0; i<9;i++) {
      setCoordenada(i, 0.0);
  }
}

void camara::setCoordenada(const int index, const float value)
{
  coordenadas[index] = value;
}

void camara::setCoordenadas(const float a, const float b, const float c, const float d, const float e, const float f, const float g, const float h, const float i)
{
  coordenadas[0] = a;
  coordenadas[1] = b;
  coordenadas[2] = c;
  coordenadas[3] = d;
  coordenadas[4] = e;
  coordenadas[5] = f;
  coordenadas[6] = g;
  coordenadas[7] = h;
  coordenadas[8] = i;

}

void camara::iniciaCamara()
{
  glLoadIdentity();
  gluLookAt(coordenadas[0], coordenadas[1],  coordenadas[2],  coordenadas[3],  coordenadas[4],  coordenadas[5],  coordenadas[6],  coordenadas[7],  coordenadas[8]);
}


