/*
 * Luz.h
 *
 *  Created on: 23/10/2014
 *      Author: salabeta
 */

#ifndef LUZ_H_
#define LUZ_H_
#include "Modelo3D.h"

class Luz {
private:
	GLfloat luzdifusa[4], luzambiente[4], luzspecular[4], posicion0[4], rotacion[4];


public:
	Luz();
	void ActivarFoco(int foco);
	void setVector4(GLfloat *v, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);

	virtual ~Luz();
};

#endif /* LUZ_H_ */
