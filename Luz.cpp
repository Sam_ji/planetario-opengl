/*
 * Luz.cpp
 *
 *  Created on: 23/0/2014
 *      Author: salabeta
 */

#include "Luz.h"

Luz::Luz() {
	
	
}



void Luz::ActivarFoco(int foco)
{
  glEnable(GL_LIGHTING);
  switch(foco){
    
      case 0: //LIGHT0
	if(glIsEnabled(GL_LIGHT0)){
		glDisable(GL_LIGHT0);
	}

	else{
	setVector4(luzdifusa, .0, 1.0, .0, 1.0);
	setVector4(luzambiente, 0.0, 0.00, 0.0, 1.0);
	setVector4(luzspecular, 0.20, 0.0, 0.0, 1.0);
	setVector4(posicion0, 50.0, -50.0, 0.0, 1.0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, luzdifusa);
	glLightfv(GL_LIGHT0, GL_AMBIENT, luzambiente);
	glLightfv(GL_LIGHT0, GL_SPECULAR, luzspecular);
	glLightfv(GL_LIGHT0, GL_POSITION, posicion0);
	glEnable(GL_LIGHT0);
	}
	break;
	
      case 1://LIGHT1
	if(glIsEnabled(GL_LIGHT1))
	  glDisable(GL_LIGHT1);
	else{
	setVector4(luzdifusa, 1.0, 0.0, .0, 0.0);
	setVector4(luzambiente, 0.0, 0.00, 0.20, 1.0);
	setVector4(luzspecular, 0.0, 0.0, 0.00, 1.0);
	setVector4(posicion0, -50.0, -50.0, 0.0, 1.0);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, luzdifusa);
	glLightfv(GL_LIGHT1, GL_AMBIENT, luzambiente);
	glLightfv(GL_LIGHT1, GL_SPECULAR, luzspecular);
	glLightfv(GL_LIGHT1, GL_POSITION, posicion0);
	glEnable(GL_LIGHT1);
	}
	break;
      case 2://LIGHT2
	if(glIsEnabled(GL_LIGHT2))
	  glDisable(GL_LIGHT2);
	else{
	setVector4(luzdifusa, .0, 1.0, .0, 1.0);
	setVector4(luzambiente, 0.30, 0.30, 0.20, 1.0);
	setVector4(luzspecular, 0.0, 0.20, 0.30, 1.0);
	setVector4(posicion0, -50.0, -0.0, 50.0, 1.0);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, luzdifusa);
	glLightfv(GL_LIGHT2, GL_AMBIENT, luzambiente);
	glLightfv(GL_LIGHT2, GL_SPECULAR, luzspecular);
	glLightfv(GL_LIGHT2, GL_POSITION, posicion0);
	glEnable(GL_LIGHT2);
	}
	
      case 3://LIGHT3
	if(glIsEnabled(GL_LIGHT3))
	  glDisable(GL_LIGHT3);
	else{
	setVector4(luzdifusa, .0, 0.0, .7, 0.0);
	setVector4(luzambiente, 0.0, 0.0, 0.20, 1.0);
	setVector4(luzspecular, 0.0, 0.0, 0.0, 1.0);
	setVector4(posicion0, 50.0, -0.0, -50.0, 0.0);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, luzdifusa);
	glLightfv(GL_LIGHT3, GL_AMBIENT, luzambiente);
	glLightfv(GL_LIGHT3, GL_SPECULAR, luzspecular);
	glLightfv(GL_LIGHT3, GL_POSITION, posicion0);
	glEnable(GL_LIGHT3);
	}
	break;
  }
    
}



void Luz::setVector4(GLfloat *v, GLfloat v0, GLfloat v1, GLfloat v2,
		GLfloat v3) {
	v[0] = v0;
	v[1] = v1;
	v[2] = v2;
	v[3] = v3;
}

Luz::~Luz() {
}

