#include "mundo.h"

mundo::mundo() {

	mundo::tiempo = 0;
	//SOL
	Modelo3D Sol;
	Sol.setEscala(0.00147);
	Sol.setPosicion(0.0,0.0,0.0);
	Sol.setSelfRotSpeed(0.4);
	mundo::planetas.push_back(Sol);

	//MERCURIO
	Modelo3D mercurio;
	mercurio.setEscala(0.00024);
	mercurio.setPosicion(700.0,0.0,0.0);
	mercurio.setPeriodo(0.24);
	mercurio.setSelfRotSpeed(3.0);
	mundo::planetas.push_back(mercurio);

	//VENUS
	Modelo3D venus;
	venus.setEscala(0.00052);
	venus.setPosicion(500.0,0.0,0.0);
	venus.setPeriodo(0.6);
	venus.setSelfRotSpeed(2.5);
	mundo::planetas.push_back(venus);

	//TIERRA
	Modelo3D tierra;
	tierra.setEscala(0.00063);
	tierra.setPosicion(600.0,0.0,0.0);
	tierra.setPeriodo(1);
	tierra.setSelfRotSpeed(1.0);
	mundo::planetas.push_back(tierra);

	//MARTE
	Modelo3D marte;
	marte.setEscala(0.00034);
	marte.setPosicion(1500.0,0.0,0.0);
	marte.setPeriodo(1.8);
	marte.setSelfRotSpeed(3.3);
	mundo::planetas.push_back(marte);

	//JUPITER
	Modelo3D jupiter;
	jupiter.setEscala(0.000867);
	jupiter.setPosicion(750,0.0,0.0);
	jupiter.setPeriodo(11.86);
	jupiter.setSelfRotSpeed(0.5);
	mundo::planetas.push_back(jupiter);

	//SATURNO
	Modelo3D saturno;
	saturno.setEscala(0.000767);
	saturno.setPosicion(1050,0.0,0.0);
	saturno.setPeriodo(29.46);
	saturno.setSelfRotSpeed(1.2);
	mundo::planetas.push_back(saturno);

	//URANO
	Modelo3D urano;
	urano.setEscala(0.00053);
	urano.setPosicion(1750.0,0.0,0.0);
	urano.setPeriodo(84.01);
	urano.setSelfRotSpeed(0.5);
	mundo::planetas.push_back(urano);

	//NEPTUNO
	Modelo3D neptuno;
	neptuno.setEscala(0.00053);
	neptuno.setPosicion(1950.0,0.0,0.0);
	neptuno.setPeriodo(164.76);
	neptuno.setSelfRotSpeed(0.3);
	mundo::planetas.push_back(neptuno);




	camara c;
	c.setCoordenadas(0.0, 0.0, 100.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	mundo::camaras.push_back(c);
	mundo::camaras.at(0).iniciaCamara();
	mundo::luceritos.ActivarFoco(0);
}

void mundo::creaMundo() {

}

const std::vector<camara>& mundo::getCamaras() const {
	return camaras;
}

const Luz& mundo::getLucerito() const {
	return luceritos;
}


void mundo::mundoDisplay() {
	//El universo es negro y así se queda
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	tiempo = tiempo + 0.3;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glScalef(0.00145,0.00145,0.00145); // Scale from editor
	int c=0;
	for(int i = 0; i<4; i++){
		if(!glIsEnabled(GL_LIGHT0+i)){
			luceritos.ActivarFoco(i);
			cout<<"activando luz "<<i<<endl;
		}
	}
	for (std::vector<Modelo3D>::iterator it = planetas.begin() ; it != planetas.end(); it++){
		it->Draw_Model(it->getEscala(), tiempo);
	}
	glFlush();
	glutSwapBuffers();
}

void mundo::mundoGLinit() {
	glEnable(GL_NORMALIZE);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

}

void mundo::startMainLoop() {
	//Ñapa

	int window;
	mundoGLinit();
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1200, 800);
	glutInitWindowPosition(100, 100);
	window = glutCreateWindow("The Multiverse without A'tuin");
	glutDisplayFunc(&mundoDisplay);
	//TODO: poner un postdisplay
	glutIdleFunc(&mundoDisplay);
	glutMotionFunc(&onMotionMundo);
	glutKeyboardFunc(&tecladoMundo);
	glutMainLoop();

}

const std::vector<Modelo3D>& mundo::getPlanetas() const {
	return planetas;
}

void mundo::onMotionMundo(int x, int y) {
	/*int xold, yold;
	my_model.alfa = (my_model.alfa + (y - yold));
	my_model.beta = (my_model.beta + (x - xold));
	xold = x;
	yold = y;*/
	//TODO Cambiar el movimiento para el nuevo modelo. Ahora lo que hacemos es mover la "cámara".
	glutPostRedisplay();

}

void mundo::tecladoMundo(unsigned char key, int x, int y) {

	if (key == 49) {
		camaras.at(0).iniciaCamara();
	}
	if (key == 50) {
		camaras.at(1).iniciaCamara();
	}
	if (key == 51) {
		camaras.at(2).iniciaCamara();
	}
	if (key == 'q')
		luceritos.ActivarFoco(0);
	if (key == 'w')
		luceritos.ActivarFoco(1);
	if (key == 'e')
		luceritos.ActivarFoco(2);
	if (key == 'r')
		luceritos.ActivarFoco(3);

	usleep(1000);
}
