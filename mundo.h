#include "punto3D.h"
#include "Modelo3D.h"
#include "cara.h"
#include "Luz.h"
#include "camara.h"



class mundo{
  
  private:
   static std::vector<Modelo3D> planetas;
   static Luz luceritos;
   static std::vector<camara> camaras;
   static float tiempo;
    
  
  public:
    mundo();
    void creaMundo();

    //Glut functions
    static void tecladoMundo(unsigned char key, int x, int y);
    static void onMotionMundo( int x, int y);

    static void mundoDisplay();
    static void mundoGLinit();
    //Aquí viene toda la mandanga
    static void startMainLoop();



    //Getters & setters
    const std::vector<camara>& getCamaras() const;
	const Luz& getLucerito() const;
	const std::vector<Modelo3D>& getPlanetas() const;
};
