#include </usr/include/GL/glut.h>
#include </usr/include/GL/gl.h>
#include </usr/include/GL/glu.h>

#include "Modelo3D.h"
#include "Luz.h"
#include "mundo.h"

//XXX LINEA DE COMPILACION
//g++ main.cpp cara.cpp punto3D.cpp Modelo3D.cpp Luz.cpp camara.cpp mundo.cpp -o Multiverso -lGL -lGLU -lglut
//DESPUES: P2 nombredelmodelo

//Variables del mundo
std::vector<Modelo3D> mundo::planetas;
Luz mundo::luceritos;
std::vector<camara> mundo::camaras;
float mundo::tiempo;



int xold, yold;
int window;

int iFondo = 0;
int iDibujo = 3;
int modo = 0;
int foco = 0;
typedef enum {
	FONDO1, FONDO2, FONDO3, FONDO4, DIBUJO1, DIBUJO2, DIBUJO3, DIBUJO4, MODO1, MODO2, MODO3, MODO4
} opcionesMenu;
void init(void){

}
void onMenu(int opcion) {
	switch (opcion) {
	case FONDO1:
		iFondo = 0;
		break;
	case FONDO2:
		iFondo = 1;
		break;
	case FONDO3:
		iFondo = 2;
		break;

	}
	glutPostRedisplay();
}

void creacionMenu(void) {
	int menuFondo, menuDibujo,menuModo, menuPrincipal;
	menuFondo = glutCreateMenu(onMenu);
	glutAddMenuEntry("Negro", FONDO1);
	glutAddMenuEntry("Verde oscuro", FONDO2);
	glutAddMenuEntry("Azul oscuro", FONDO3);
	menuPrincipal = glutCreateMenu(onMenu);
	glutAddSubMenu("Color de fondo", menuFondo);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char **argv) {
	glutInit(&argc,argv);
	mundo universo;
	//TODO tenemos que ver como llamamos al menu aqui
	//creacionMenu();
	universo.startMainLoop();
}
