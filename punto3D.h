/*
 * punto3D.h
 *
 *  Created on: 09/10/2014
 *      Author: salabeta
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

// Para OpenGL
#include <GL/gl.h>
// Para GLU
#include <GL/glu.h>
// Para GLUT
#include <GL/glut.h>


#ifndef PUNTO3D_H_
#define PUNTO3D_H_

class punto3D {

private:
	float x;
	float y;
	float z;

public:
	punto3D();
	punto3D(float x, float y, float z);
	virtual ~punto3D();
	float getX() const;
	void setX(float x);
	float getY() const;
	void setY(float y);
	float getZ() const;
	void setZ(float z);
};

#endif /* PUNTO3D_H_ */
