/*
 * punto3D.cpp
 *
 *  Created on: 09/10/2014
 *      Author: salabeta
 */

#include "punto3D.h"

punto3D::punto3D() {
	setX(0.0);
	setY(0.0);
	setZ(0.0);

}

float punto3D::getX() const {
	return x;
}

void punto3D::setX(float x) {
	this->x = x;
}

float punto3D::getY() const {
	return y;
}

void punto3D::setY(float y) {
	this->y = y;
}

float punto3D::getZ() const {
	return z;
}

punto3D::punto3D(float x, float y, float z) {
	setX(x);
	setY(y);
	setZ(z);
}

void punto3D::setZ(float z) {
	this->z = z;
}

punto3D::~punto3D() {

}

