/*
 * Modelo3D.h
 *
 *  Created on: 09/10/2014
 *      Author: salabeta
 */

#ifndef MODELO3D_H_
#define MODELO3D_H_
#include <vector>
#include <string.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "punto3D.h"
#include "Luz.h"

#include "cara.h"
using namespace std;

typedef std::vector<punto3D> ListaVertex;
typedef std::vector<cara> ListaFaces;


using namespace std;
class Modelo3D {

private:
	int _NumCaras;
	int _NumVertices;
	float escala;
	float posicion[3];
	float velocidad;
	float periodo;
	float self_rot_speed;
	//TODO traslación


	

public:
	//Getters, setters, constructores y destructores
	Modelo3D();
	Modelo3D(const int ncaras, const int nvertices);
	virtual ~Modelo3D();
	int getNumCaras() const;
	void setNumCaras(int numCaras);
	int getNumVertices() const;
	void setNumVertices(int numVertices);
	void setPosicion(float a, float b, float c);

	//Métodos propios
	void Load_Model();
	void Draw_Model(float scale_from_editor, long tiempo);
	void setModelColor(float c1, float c2, float c3);
	void setVector4(GLfloat *v, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
	void setMaterialSpec(GLfloat a, GLfloat b, GLfloat c, GLfloat d );
	void setMaterialEmision(GLfloat a, GLfloat b, GLfloat c, GLfloat d );
	void preparaMaterial();
	float getEscala() const;
	void setEscala(float escala);
	const float* getPosicion() const;
	float getVelocidad() const;
	void setVelocidad(float velocidad);
	float getPeriodo() const;
	void setPeriodo(float periodo);

	float getSelfRotSpeed() const {
		return self_rot_speed;
	}

	void setSelfRotSpeed(float selfRotSpeed) {
		self_rot_speed = selfRotSpeed;
	}

	//Atributos públicos
	float alfa;
	float beta;
	float Model_color[3];
	ListaFaces ListaCaras;
	ListaVertex ListaPuntos3D;
	GLfloat material_difuso[4], material_ambiente[4], material_specular[4], material_emission[4];

};

#endif /* MODELO3D_H_ */
